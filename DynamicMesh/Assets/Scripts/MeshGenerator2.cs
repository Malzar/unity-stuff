﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode()]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[System.Serializable]
public class MeshGenerator2 : MonoBehaviour {

    public int XVertex;
    public int Yvertex;
    public float Width;
    public float Hehght;
    public Material MeshEditionMaterial;
    public Material MeshFinalMaterial;
    [Header("Perling parameters")]
    public float Scale;
    public int PerlingSeed;
    public int PerlingGridSize;
    public int PerlingOctaves;
    public float MaxHeight;
    public float MinHeight;
    [Header("High parameters")]
    public float ShortHigh;
    public float MediumHigh;
    public float TopHigh;
    public float LerpDistance;

    [SerializeField,HideInInspector]
    private List<Vector3> vertex;
    [SerializeField, HideInInspector]
    private List<Vector2> uvVertex;
    [SerializeField, HideInInspector]
    private List<Color> colorVertex;
    [SerializeField, HideInInspector]
    private Mesh GeneratedMesh=null;

    private PerlingNoise2D PerlingNoise;

    private void Awake() {
        Initialize();
    }

    private void Initialize() {
        if(vertex==null)
            vertex = new List<Vector3>();
        if (uvVertex == null)
            uvVertex = new List<Vector2>();
        if (colorVertex == null)
            colorVertex = new List<Color>();
    }

    internal void GenerateMesh() {
        GeneratePerlingNoise();
        GenerateVertex();
        GenerateMeshElement(vertex.ToArray(), uvVertex.ToArray());
        TriangularizateMesh();
        CalculateMeshNormals();
        SetMaterialParameters(MeshEditionMaterial);
        SetMaterial(MeshEditionMaterial);
    }

    internal void EndEdition() {
        ClearHandles();
        CalculateHightColor();
        SetMaterialParameters(MeshFinalMaterial);
        DestroyImmediate(GetComponent<MeshGenerator>());
    }

    private void SetMaterialParameters(Material mat) {
        mat.SetFloat("_ShortHigh", ShortHigh);
        mat.SetFloat("_MediumHigh", MediumHigh);
        mat.SetFloat("_TopHigh", TopHigh);
        mat.SetFloat("_LerpDistance", LerpDistance);
    }

    private void GeneratePerlingNoise() {        
        PerlingNoise = new PerlingNoise2D(PerlingGridSize, PerlingSeed);
    }

    private void GenerateVertex() {
        Vector3 pos;
        Vector2 uv;
        float xSpace = (Width / (XVertex - 1));
        float ySpace = (Hehght / (Yvertex - 1));
        float halfWidth = Width / 2.0f;
        float halfHeight = Hehght / 2.0f;
        float noise;

        Scale = Scale > 1 ? Scale : 1;

        for (int j = 0; j < Yvertex; j++) {
            for (int i = 0; i < XVertex; i++) {
                noise = PerlingNoise.getNoise(((i * xSpace) / Width)* Scale, ((j * ySpace) / Hehght)* Scale, PerlingOctaves);
                pos = new Vector3(i * xSpace,
                    (noise>=0)?noise * MaxHeight:noise*MinHeight,
                    j * ySpace);
                vertex.Add(pos);

                uv = new Vector2(((i * xSpace) - halfWidth) / halfWidth, ((j * ySpace) - halfHeight) / halfHeight);
                uvVertex.Add(uv);                
                GenerateHandle(i + j * XVertex, pos);
            }
        }
    }

    private void GenerateMeshElement(Vector3[] vertex, Vector2[] uvVertex = null) {
        if (GeneratedMesh == null) {
            GeneratedMesh = new Mesh();
            this.gameObject.GetComponent<MeshFilter>().mesh = GeneratedMesh;
        }

        GeneratedMesh.vertices = vertex;
        if (uvVertex != null)
            GeneratedMesh.uv = uvVertex;
            
    }

    private void TriangularizateMesh() {
        List<int> triangulation = new List<int>();
        for (int j = 0; j < Yvertex - 1; j++) {
            for (int i = 0; i < XVertex - 1; i++) {
                triangulation.Add(i + (j * XVertex) + XVertex);
                triangulation.Add(i + (j * XVertex) + 1);
                triangulation.Add(i + (j * XVertex));

                triangulation.Add(i + (j * XVertex) + 1);
                triangulation.Add(i + (j * XVertex) + XVertex);
                triangulation.Add(i + (j * XVertex) + XVertex + 1);
            }
        }
        GeneratedMesh.triangles = triangulation.ToArray();
        triangulation.Clear();
    }

    private void CalculateMeshNormals() {
        GeneratedMesh.RecalculateNormals();
    }

    private void SetMaterial(Material mat) {
        this.gameObject.GetComponent<MeshRenderer>().material = mat;
    }

    private void CalculateHightColor() {
        float height;
        for (int j = 0; j < Yvertex; j++) {
            for (int i = 0; i < XVertex; i++) {
                height = vertex[i + j * XVertex].y;
                ColoreateVertex(i + j * XVertex, height);
            }
        }
        GeneratedMesh.colors = colorVertex.ToArray();
    }

    private void ColoreateVertex(int pos, float height) {
        Color colorV;
        colorV = GetHeightColor(height);

        if (colorVertex.Count<=pos) {
            colorVertex.Add(colorV);
        } else {
            colorVertex[pos] = colorV;
        }
    }

    private Color GetHeightColor(float height) {
        Color vertexColor= Color.black;
        float halfLerpDistance = LerpDistance * 0.5f;

        if(height <= (ShortHigh - halfLerpDistance))
            vertexColor = new Vector4(1.0f,0.0f,0.0f,0.0f);

        if(height> (ShortHigh - halfLerpDistance) && (height <= ShortHigh + halfLerpDistance)) {
            float lerp = (height - (ShortHigh - halfLerpDistance))/LerpDistance;
            vertexColor = new Vector4(1.0f, 0.0f, 0.0f, 0.0f) * (1-lerp)+
                           new Vector4(0.0f, 1.0f, 0.0f, 0.0f) * (lerp);
        }

        if (height > (ShortHigh + halfLerpDistance) && height<= (MediumHigh - halfLerpDistance))
            vertexColor = new Vector4(0.0f, 1.0f, 0.0f, 0.0f);

        if (height > (MediumHigh - halfLerpDistance) && (height <= MediumHigh + halfLerpDistance)) {
            float lerp = (height - (MediumHigh - halfLerpDistance)) / LerpDistance;
            vertexColor = new Vector4(0.0f, 1.0f, 0.0f, 0.0f)* (1 - lerp) + 
                            new Vector4(0.0f,0.0f, 1.0f, 0.0f) * (lerp);
        }

        if (height > (MediumHigh + halfLerpDistance) && height <= (TopHigh - halfLerpDistance))
            vertexColor = new Vector4(0.0f, 0.0f, 1.0f, 0.0f);

        if (height > (TopHigh - halfLerpDistance) && (height <= TopHigh + halfLerpDistance)) {
            float lerp = (height - (TopHigh - halfLerpDistance)) / LerpDistance;
            vertexColor = new Vector4(0.0f, 0.0f, 1.0f, 0.0f) * (1 - lerp) +
                        new Vector4(0.0f, 0.0f, 0.0f, 1.0f) * (lerp);
        }

        if (height > (TopHigh + halfLerpDistance))
            vertexColor = new Vector4(0.0f, 0.0f, 0.0f, 1.0f);

        return vertexColor;
    }

    private void GenerateHandle(int index, Vector3 position) {
        GameObject handle = new GameObject();
        handle.transform.SetParent(this.transform);
        handle.name = "Vertex-" + index.ToString();
        handle.transform.localPosition = position;
        AssignLabel(handle);
    }

    private void AssignLabel(GameObject g) {
        Texture2D tex = EditorGUIUtility.IconContent("sv_icon_dot0_pix16_gizmo").image as Texture2D;
        Type editorGUIUtilityType = typeof(EditorGUIUtility);
        BindingFlags bindingFlags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
        object[] args = new object[] { g, tex };
        editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);
    }

    private void ClearHandles() {
        for (int i = transform.childCount; i > 0; i--) {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    public void CleanMesh() {
        if (vertex != null)
            vertex.Clear();
        if (uvVertex != null)
            uvVertex.Clear();
        if (colorVertex != null)
            colorVertex.Clear();
        if (GeneratedMesh != null)
            GeneratedMesh.Clear();
        if(transform.childCount>0)
            ClearHandles();
    }

    private void Update() {
        String name;
        int pos;
        bool mustRecalculate = false;
        foreach(GameObject go in Selection.gameObjects) {
            if (go == this.gameObject) {
                SetMaterialParameters(MeshEditionMaterial);
            }

            if (go.name.Contains("Vertex-")) {
                name = go.name;
                name = name.Substring(name.IndexOf('-')+1);
                int.TryParse(name, out pos);
                vertex[pos] = go.transform.localPosition;
                mustRecalculate = true;
            }
        }
        if (mustRecalculate) {
            GenerateMeshElement(vertex.ToArray());
            CalculateMeshNormals();
        }
    } 
}