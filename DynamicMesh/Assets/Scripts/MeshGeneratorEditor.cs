﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[InitializeOnLoad]
[CustomEditor(typeof(MeshGenerator))]
public class MeshGeneratorEditor : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        MeshGenerator myScript = (MeshGenerator) target;
        if (GUILayout.Button("Build Object")) {
            if (myScript.XVertex > 1 && myScript.Yvertex > 1) {
                myScript.CleanMesh();
                myScript.GenerateMesh();
            } else {
                Debug.Log("You must set a corret number of X and Y division");
            }
        }

        if (GUILayout.Button("End edition")) {
            myScript.EndEdition();
        }

        if (GUI.changed && !Application.isPlaying) {
            EditorUtility.SetDirty(myScript);
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }
}
