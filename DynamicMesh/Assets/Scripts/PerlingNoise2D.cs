﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlingNoise2D {

    private float[][] gridSeed;
    private int gridSize;

    public PerlingNoise2D(int Size, int seed = 0) {
        gridSize = (Size > 1) ? Size : 1;
        gridSeed = new float[gridSize][];
        Random.InitState(seed);
        for (int i = 0; i < gridSize; i++) {
            gridSeed[i] = new float[gridSize];
            for (int j = 0; j < gridSize; j++) {
                gridSeed[i][j] = Random.Range(-float.MaxValue, float.MaxValue) / float.MaxValue;
            }
        }
    }

    public float getNoise(float xPos, float yPos, int octaves = 1) {
        float noise = 0;
        float amplitude = 1;
        double frecuence =1;
        float weigthedAverage = 0;

        octaves = octaves < 2 ? 2 : octaves;

        for (int i = 0; i < octaves; i++) {
            noise += perlin(xPos * (float)frecuence, yPos * (float)frecuence) * amplitude;

            weigthedAverage += amplitude;
            frecuence *= 2.0f;
            amplitude *= 0.5f;
        }
        return noise/ weigthedAverage;
    }

    float perlin(float xPos, float yPos) {
        float biInterpolationX, biInterpolationY;
        float sample1, sample2, sample3, sample4;
        int x, y;

        x = (int) xPos % gridSize;
        y = (int) yPos % gridSize;
        biInterpolationX = Mathf.Repeat(xPos, 1.0f);
        biInterpolationY = Mathf.Repeat(yPos, 1.0f);

        //bilineal interpolation
        sample1 = gridSeed[x][y] * (1 - biInterpolationX) * (1 - biInterpolationY);
        sample2 = gridSeed[(x + 1) % gridSize][y] * (biInterpolationX) * (1 - biInterpolationY);
        sample3 = gridSeed[x][(y + 1) % gridSize] * (1 - biInterpolationX) * (biInterpolationY);
        sample4 = gridSeed[(x + 1) % gridSize][(y + 1) % gridSize] * (biInterpolationX) * (biInterpolationY);

        return sample1+sample2+sample3+sample4;
    }
}
