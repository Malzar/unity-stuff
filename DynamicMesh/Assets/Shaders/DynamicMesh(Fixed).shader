﻿Shader "Malzar/DynamicMesh/FixedPosition" {
	Properties {
		_ShortTex ("Short texture (RGB)", 2D) = "white" {}
		_MShortTex ("Middle short texture (RGB)", 2D) = "white" {}
		_MTopTex ("Middle Top texture (RGB)", 2D) = "white" {}
		_TopTex ("Top texture (RGB)", 2D) = "white" {}
		_VerticalText("Vertical texture (RGB)",2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows
		#pragma vertex vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _ShortTex;
		sampler2D _MShortTex;
		sampler2D _MTopTex;
		sampler2D _TopTex;
		sampler2D _VerticalText;

		struct appdata{
			float4 vertex :POSITION;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
			float4 color:COLOR;
		};

		struct Input {
			float2 uv_TopTex;
			float2 uv_MTopTex;
			float2 uv_MShortTex;
			float2 uv_ShortTex;
			float2 uv_VerticalText;
			fixed4 hColor;
		};

			
		void vert (inout appdata v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.hColor = v.color;
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			half lerpC =dot(o.Normal,float3(0,1,0));
			lerpC*=lerpC;
			fixed4 verticalColor = tex2D(_VerticalText,IN.uv_VerticalText);

			fixed4 horizontalColor;
			horizontalColor = tex2D (_ShortTex, IN.uv_ShortTex) * IN.hColor.r;

			horizontalColor += lerp(verticalColor,tex2D (_MShortTex, IN.uv_MShortTex),lerpC) * IN.hColor.g;
			horizontalColor += lerp(verticalColor,tex2D (_MTopTex, IN.uv_MTopTex),lerpC) * IN.hColor.b;

			horizontalColor += tex2D (_TopTex, IN.uv_TopTex) * IN.hColor.a;
			//horizontalColor.Albedo = c.rgb;

			

			o.Albedo = horizontalColor.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = 0;
			o.Smoothness = 0;
			o.Alpha = verticalColor.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
