﻿Shader "Malzar/DynamicMesh/NotFixedPosition" {
	Properties {
		_ShortHigh("Short high",float) = 0.0
		_MediumHigh("Medium high",float) = 2.0
		_TopHigh("Top high",float) = 4.0
		_LerpDistance("Lerp distance", float)= 1.0
		_ShortTex ("Short texture (RGB)", 2D) = "white" {}
		_MShortTex ("Middle short texture (RGB)", 2D) = "white" {}
		_MTopTex ("Middle Top texture (RGB)", 2D) = "white" {}
		_TopTex ("Top texture (RGB)", 2D) = "white" {}
		_VerticalText("Vertical texture (RGB)",2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows
		#pragma vertex vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _ShortTex;
		sampler2D _MShortTex;
		sampler2D _MTopTex;
		sampler2D _TopTex;
		sampler2D _VerticalText;

		fixed _ShortHigh;
		fixed _MediumHigh;
		fixed _TopHigh;
		fixed _LerpDistance;

		struct appdata{
			float4 vertex :POSITION;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
			float4 color:COLOR;
		};

		struct Input {
			float2 uv_TopTex;
			float2 uv_MTopTex;
			float2 uv_MShortTex;
			float2 uv_ShortTex;
			float2 uv_VerticalText;
			fixed4 hMap;
		};

			
		void vert (inout appdata v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input,o);
			fixed halfLerpDistance = _LerpDistance*0.5;
			float height = v.vertex.y;
			float lerpHMi,lerpHMe,lerpHTo;

			lerpHMi = (height - (_ShortHigh - halfLerpDistance)) / _LerpDistance;
			lerpHMe = (height - (_MediumHigh - halfLerpDistance)) / _LerpDistance;
			lerpHTo = (height - (_TopHigh - halfLerpDistance)) / _LerpDistance;

			o.hMap.r = (height <= (_ShortHigh - halfLerpDistance)) +
			(height> (_ShortHigh - halfLerpDistance) && (height <= _ShortHigh + halfLerpDistance))*(1-lerpHMi);

			o.hMap.g =(height> (_ShortHigh - halfLerpDistance) && (height <= _ShortHigh + halfLerpDistance))*lerpHMi+
			((height > _ShortHigh + halfLerpDistance)&&(height <= _MediumHigh - halfLerpDistance))+
			((height > _MediumHigh - halfLerpDistance)&&(height <= _MediumHigh + halfLerpDistance))*(1-lerpHMe);

			o.hMap.b = (height> (_MediumHigh - halfLerpDistance) && (height <= _MediumHigh + halfLerpDistance))*lerpHMe+
			((height > _MediumHigh + halfLerpDistance)&&(height <= _TopHigh - halfLerpDistance))+
			((height > _TopHigh - halfLerpDistance)&&(height <= _TopHigh + halfLerpDistance))*(1-lerpHTo);

			o.hMap.a = ((height > _TopHigh - halfLerpDistance)&&(height <= _TopHigh + halfLerpDistance))*lerpHTo+ 
			(height > (_TopHigh + halfLerpDistance));
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			half lerpC =dot(o.Normal,float3(0,1,0));
			lerpC*=lerpC;
			fixed4 verticalColor = tex2D(_VerticalText,IN.uv_VerticalText);

			fixed4 horizontalColor;
			horizontalColor = tex2D (_ShortTex, IN.uv_ShortTex) * IN.hMap.r;

			horizontalColor += lerp(verticalColor,tex2D (_MShortTex, IN.uv_MShortTex),lerpC) * IN.hMap.g;
			horizontalColor += lerp(verticalColor,tex2D (_MTopTex, IN.uv_MTopTex),lerpC) * IN.hMap.b;

			horizontalColor += tex2D (_TopTex, IN.uv_TopTex) * IN.hMap.a;
			//horizontalColor.Albedo = c.rgb;

			

			o.Albedo = horizontalColor.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = 0;
			o.Smoothness = 0;
			o.Alpha = verticalColor.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
