﻿using System;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

[InitializeOnLoad]
[CustomEditor(typeof(BezierPath))]
public class BezierPathEditor : Editor {

    private BezierPath p_Path;
    private Transform p_HandleTransform;
    private int p_IndexCPSelected;
    private int p_IndexWPSelected;
    private string p_ScriptablePathName = "Scriptable object file name";
    private ScriptablePath p_ScriptablePathSelected;
    private Vector3 p_PointSelectedValue;
    private Vector3 p_newPointSelectedValue;
    private float p_WaypointSize = 0.5f;
    private Color p_WaypointColor = Color.red * 0.9f;
    private float p_ControlWaypointSize = 0.25f;
    private Color p_ControlWaypointColor = Color.yellow * 0.9f;
    private Color p_BezierLineColor = Color.green * 0.9f;
    private Color p_DirectionLineColor = Color.cyan * 0.9f;
    private bool p_EditWPMode;
    private bool p_ShowDirectionArrow = false;
    private bool p_PointSelected = false;

    private const string FOLDERPATH = "Assets/Resources/Paths/";

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        p_Path = (BezierPath) target;

        EditorGUILayout.BeginHorizontal();
        p_WaypointSize = EditorGUILayout.FloatField("Waypoint size:", p_WaypointSize);
        p_WaypointColor = EditorGUILayout.ColorField("Color:", p_WaypointColor);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        p_ControlWaypointSize = EditorGUILayout.FloatField("Control waypoint size:", p_ControlWaypointSize);
        p_ControlWaypointColor = EditorGUILayout.ColorField("Color:", p_ControlWaypointColor);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        p_BezierLineColor = EditorGUILayout.ColorField("Bezier line color:", p_BezierLineColor);
        p_DirectionLineColor = EditorGUILayout.ColorField("Direction line color:", p_DirectionLineColor);
        EditorGUILayout.EndHorizontal();
        p_ShowDirectionArrow = EditorGUILayout.Toggle("Show direction arrow:", p_ShowDirectionArrow);

        GUILayout.Space(15);

        GUILayout.Label("Path options");
        if(p_PointSelected) {
            p_PointSelectedValue = EditorGUILayout.Vector3Field("Point selected:", p_PointSelectedValue);
            if(p_EditWPMode) {
                p_Path.SetWayPoint(p_IndexWPSelected, p_PointSelectedValue);
            } else {
                p_Path.SetControllWayPoint(p_IndexCPSelected, p_PointSelectedValue);
            }
        }

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Add waypoint")) {
            p_Path.AddWayPoint();
        }

        if(GUILayout.Button("Set sapwnpoint")) {
            p_Path.SetSpawnPoint();
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();

        if(GUILayout.Button("Remove wayPoint")) {
            p_Path.RemoveWayPoint(p_IndexWPSelected);
        }

        if(GUILayout.Button("Remove all wayPoints")) {
            p_Path.ResetWayPoint();
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(15);

        GUILayout.Label("Scriptable object generation");
        if(GUILayout.Button("New path")) {
            p_Path.Initialize();
        }

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("File name");
        p_ScriptablePathName = GUILayout.TextField(p_ScriptablePathName);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Load scriptable path")) {
            LoadScriptableObjectPath();
        }

        if(GUILayout.Button("Export as scriptableObject")) {
            p_Path.CalculatePathLegth();
            GenerateScriptableObjectPath();
        }
        EditorGUILayout.EndHorizontal();

        if(GUI.changed && !Application.isPlaying) {
            EditorUtility.SetDirty(p_Path);
        }
    }

    private void LoadScriptableObjectPath() {
        string path = EditorUtility.OpenFilePanel("Load Scriptable Path", FOLDERPATH, "asset");

        if(path != "") {
            string [] result;
            result = path.Split(new string [] { "Assets" }, StringSplitOptions.None);
            path = path.Replace(result [0], "");

            p_ScriptablePathSelected = AssetDatabase.LoadAssetAtPath(path, typeof(ScriptablePath)) as ScriptablePath;
            p_Path.LoadScriptablePath(p_ScriptablePathSelected);
            p_ScriptablePathName = p_ScriptablePathSelected.name;
        }
    }

    private void OnSceneGUI() {
        if(p_Path == null)
            p_Path = target as BezierPath;
        p_HandleTransform = p_Path.transform;
        if(p_Path.Waypoints != null) {
            if(p_Path.Waypoints.Count > 0) {
                for(int i = 0; i < p_Path.Waypoints.Count - 1; i++) {
                    DrawControllLine(i, 2 * i);
                    DrawControllLine(i + 1, 2 * i + 1);
                    DrawControlPoint(2 * i);
                    DrawControlPoint(2 * i + 1);
                    DrawWayPoint(i);

                    DrawBezierCurve(i, i + 1);
                }
                DrawWayPoint(p_Path.Waypoints.Count - 1);
            }
        }
    }

    private void DrawWayPoint(int index) {
        Handles.color = p_WaypointColor;

        Vector3 point = p_Path.Waypoints [index];

        if(Handles.Button(p_Path.Waypoints [index], Quaternion.identity, p_WaypointSize, p_WaypointSize * 1.1f, Handles.DotHandleCap)) {
            p_IndexWPSelected = index;
            p_EditWPMode = true;
            p_PointSelected = true;
            Repaint();
        }

        if(p_IndexWPSelected == index && p_EditWPMode) {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, Quaternion.identity);
            p_PointSelectedValue = point;
            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(p_Path, "Move controll point");
                p_Path.SetWayPoint(p_IndexWPSelected, point);
                EditorUtility.SetDirty(p_Path);
            }
        }
    }

    private void DrawControlPoint(int index) {
        Handles.color = p_ControlWaypointColor;
        Vector3 point = p_Path.ControlWaypoints [index];

        if(Handles.Button(p_Path.ControlWaypoints [index], Quaternion.identity, p_ControlWaypointSize, p_ControlWaypointSize * 1.1f, Handles.DotHandleCap)) {
            p_IndexCPSelected = index;
            p_EditWPMode = false;
            p_PointSelected = true;
            Repaint();
        }

        if(p_IndexCPSelected == index && !p_EditWPMode) {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, Quaternion.identity);
            p_PointSelectedValue = point;
            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(p_Path, "Move controll point");
                p_Path.SetControllWayPoint(p_IndexCPSelected, point);
                EditorUtility.SetDirty(p_Path);
            }
        }
    }

    private void DrawControllLine(int index0, int index1) {
        Handles.color = Color.white;
        Handles.DrawLine(p_Path.Waypoints [index0], p_Path.ControlWaypoints [index1]);
    }

    private void DrawBezierCurve(int indexWaypoint0, int indexWaypoint1) {
        float increment = 1.0f / p_Path.SegmentCount;
        Vector3 lastSegmentPosition = p_Path.Waypoints [indexWaypoint0];
        Vector3 newSegmentPosition;
        Vector3 p0 = p_Path.Waypoints [indexWaypoint0];
        Vector3 p1 = p_Path.ControlWaypoints [indexWaypoint0 * 2];
        Vector3 p2 = p_Path.ControlWaypoints [indexWaypoint0 * 2 + 1];
        Vector3 p3 = p_Path.Waypoints [indexWaypoint1];

        Vector3 direction;
        for(int i = 0; i <= p_Path.SegmentCount; i++) {
            Handles.color = p_BezierLineColor;
            newSegmentPosition = Bezier.CalculateBecierInterpolation(p0, p1, p2, p3, increment * i);
            Handles.DrawLine(lastSegmentPosition, newSegmentPosition);
            if(p_ShowDirectionArrow) {
                Handles.color = p_DirectionLineColor;
                direction = Bezier.CalculateDirectionInterpolation(p0, p1, p2, p3, increment * i);
                Handles.DrawLine(lastSegmentPosition, newSegmentPosition + direction);
            }
            lastSegmentPosition = newSegmentPosition;
        }
    }

    private void GenerateScriptableObjectPath() {
        ScriptablePath asset = CreateInstance<ScriptablePath>();
        asset.Waypoints = p_Path.Waypoints;
        asset.ControlWaypoints = p_Path.ControlWaypoints;
        asset.SplinesLengths = p_Path.SplinesLegths;
        asset.PathLenght = p_Path.PathLegth;

        string path = Application.dataPath + "/Resources";
        if(!Directory.Exists(path)) {
            Directory.CreateDirectory(path);

        }
        path = path + "/Paths";
        if(!Directory.Exists(path)) {
            Directory.CreateDirectory(path);
        }
        string assetPathName = FOLDERPATH + p_ScriptablePathName + ".asset";
        AssetDatabase.CreateAsset(asset, assetPathName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
