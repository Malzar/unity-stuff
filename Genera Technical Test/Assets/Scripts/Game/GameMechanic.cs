﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMechanic : MonoBehaviour {
    public static GameMechanic Instance;

    [SerializeField]
    private List<ScriptableLevel> p_LevelsPropierties;
    private List<ZombieController> p_ZombiesSpawned;
    private int p_LevelToLoad;
    private float p_TimeElapsed;
    private float p_SpawnTimeElapsed;
    private float p_LevelDuration;
    private float p_SpawnTime;
    private bool p_SpawnZombies;
    [SerializeField]
    private float p_PlayerLife;

    private void Awake() {
        if(Instance == null) {
            Instance = this;
        } else if(Instance != this) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        RegisterListeners();
    }

    private void RegisterListeners() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //Only load level when isn't a menu
    private void OnSceneLoaded(Scene scene, LoadSceneMode arg1) {
        if(scene.name != SceneLoader.Instance.GetSceneName(SCENES.Menu)) {
            Initialize();
        }
    }

    private void Initialize() {
        p_SpawnTimeElapsed = 0;
        p_TimeElapsed = 0;
        p_ZombiesSpawned = new List<ZombieController>();
        p_SpawnZombies = true;
        p_LevelDuration = p_LevelsPropierties [p_LevelToLoad].LevelsTimeInSecond;
        //Plus one because time delta is not enough precise and never can spawn the last zombie.
        p_SpawnTime = p_LevelDuration / (p_LevelsPropierties [p_LevelToLoad].TotalZombieSpawned+1);
        p_PlayerLife = p_LevelsPropierties [p_LevelToLoad].PlayerLife;
    }

    public void SetLevelToLoad(int level) {
        if(level > p_LevelsPropierties.Count)
            throw new System.IndexOutOfRangeException("This level doesn't exist");
        p_LevelToLoad = level;
    }

    public void PlayerWin() {
        p_LevelToLoad++;
        if(p_LevelToLoad < p_LevelsPropierties.Count) {
            Debug.Log("Next level");
            SceneLoader.Instance.LoadScene(SceneLoader.Instance.GetLevelScene(p_LevelToLoad), 5.0f);
        } else {
            Debug.Log("You win");
            SceneLoader.Instance.LoadScene(SCENES.Menu, 5.0f);
        }
    }

    public void PlayerDeath() {
        Debug.Log("You lose");
        SceneLoader.Instance.LoadScene(SCENES.Menu, 5.0f);
    }

    private void ZombieAttack(float damage) {
        p_PlayerLife -= damage;
        if(p_PlayerLife <= 0)
            PlayerDeath();
    }

    private void ZombieDeath(ZombieController zombie) {
        p_ZombiesSpawned.Remove(zombie);
        zombie.OnZombieDead -= ZombieDeath;
        zombie.OnZombieAttack -= ZombieAttack;
        if(p_ZombiesSpawned.Count == 0 && p_TimeElapsed >= p_LevelDuration)
            PlayerWin();
    }

    void Update() {
        if(p_SpawnZombies) {
            if(SpawnTime()) {
                SpawnZombie();
            }
            IncrementTime();
        }

    }

    private void SpawnZombie() {
        ZombieController zombie = ZombieFactory.Instance.SpawnZombie();
        p_ZombiesSpawned.Add(zombie);
        zombie.OnZombieDead += ZombieDeath;
        zombie.OnZombieAttack += ZombieAttack;
    }

    private bool SpawnTime() {
        p_SpawnTimeElapsed += Time.deltaTime;
        if(p_SpawnTimeElapsed >= p_SpawnTime) {
            p_SpawnTimeElapsed = 0;
            return true;
        } else {
            return false;
        }
    }

    private void IncrementTime() {
        p_TimeElapsed += Time.deltaTime;
        p_SpawnZombies = p_TimeElapsed < p_LevelDuration;
    }
}
