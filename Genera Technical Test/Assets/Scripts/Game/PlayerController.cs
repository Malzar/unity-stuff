﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private float p_LineWith = 0.1f;
    [SerializeField]
    private Material p_LineMat;
    [SerializeField]
    private Transform p_Camera;
    [SerializeField]
    private bool p_CanRotate;

    private float p_SpeedH = 2.0f;
    private float p_SpeedV = 2.0f;
    [SerializeField]
    private float p_ShootCoolDown = 0.1f;
    private float p_Yaw;
    private float p_Pitch;
    private float p_coolingTime;
    private int p_EnemyMask;

    private void Awake() {
        Initialize();
    }

    private void Initialize() {
        p_CanRotate = true;
        p_Pitch = transform.rotation.eulerAngles.x;
        p_Yaw = transform.rotation.eulerAngles.y;
        p_EnemyMask = LayerMask.GetMask("Enemy");
        if(p_Camera == null)
            p_Camera = Camera.main.transform;
        p_Pitch = p_Camera.rotation.eulerAngles.x;
        p_Yaw = p_Camera.rotation.eulerAngles.y;
    }

    private void RotateCamera() {
        p_Pitch += p_SpeedV * Input.GetAxis("Mouse Y");
        p_Yaw += p_SpeedH * Input.GetAxis("Mouse X");

        p_Camera.eulerAngles = new Vector3(p_Pitch, p_Yaw, 0.0f);
    }

    public void EnableRotation(bool enable) {
        p_CanRotate = enable;
    }

    private void Shoot() {
        Ray shot = new Ray(p_Camera.position, p_Camera.forward);
        RaycastHit enemyHit;

        if(Physics.Raycast(shot, out enemyHit, 50)) {
            if(enemyHit.transform.gameObject.layer == LayerMask.NameToLayer("Enemy")) {
                enemyHit.transform.gameObject.GetComponent<ZombieColliderControler>().HitZombie(5.0f, enemyHit.point);
            }
        }

        DrawShoot();
    }

    private void DrawShoot() {
        float dissapearTime = 2.0f;
        GameObject lineGO = new GameObject();
        LineRenderer lr = lineGO.AddComponent<LineRenderer>();
        lr.material = p_LineMat;
        lr.SetPositions(new Vector3 [] { p_Camera.transform.position+Vector3.down*0.5f,
                                        p_Camera.forward * 50+p_Camera.position});
        lr.startWidth = p_LineWith;
        lr.endWidth = p_LineWith;
        lr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        lineGO.AddComponent<ShotTailController>().Initialize(dissapearTime*0.9f, lr.material);

        Destroy(lineGO, dissapearTime);
        Debug.DrawRay(p_Camera.position, p_Camera.forward * 50, Color.white, 10.0f);
    }

    // Update is called once per frame
    void Update() {
        if(p_CanRotate) {
            RotateCamera();
            if(Input.GetKey(KeyCode.Mouse0)) {
                if(p_coolingTime == 0) {
                    Shoot();
                }
                p_coolingTime += Time.deltaTime;
                if(p_coolingTime >= p_ShootCoolDown)
                    p_coolingTime = 0;
            }
        }
    }
}

