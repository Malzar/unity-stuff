﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotTailController : MonoBehaviour
{
    private float p_TimeToDissapear=2.0f;
    private float p_TimeElipsed;
    private Material p_Mat;

    public void Initialize(float time, Material mat) {
        p_TimeElipsed = 0;
        p_TimeToDissapear = time;
        p_Mat = mat;
    }

    // Update is called once per frame
    void Update()
    {
        if(p_TimeElipsed < p_TimeToDissapear) {
            float percentaje = 1 - ( p_TimeElipsed/ p_TimeToDissapear);
            //Quadratic loss
            p_Mat.SetFloat("_Alpha", percentaje*percentaje);
            p_TimeElipsed += Time.deltaTime;
        }
    }
}
