﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ZombieAnimatorController : MonoBehaviour
{
    [SerializeField,HideInInspector]
    private ZombieState p_State;
    [SerializeField,HideInInspector]
    private Animator p_Animator;

    public delegate void ScreamEnd();
    public event ScreamEnd OnScreamEnd; 

    void Awake() {
        Initialize();
    }

    private void Initialize() {
        if(p_Animator == null)
            p_Animator = gameObject.GetComponent<Animator>();
    }

    public void SetAnimatorController(RuntimeAnimatorController animatorController) {
        p_Animator.runtimeAnimatorController = animatorController;
    }

    public bool IsScreamAnimation() {
        return p_Animator.GetCurrentAnimatorStateInfo(0).IsName("scream");
    }

    public void Play() {
        p_Animator.enabled = true;
    }

    public void Stop() {
        p_Animator.enabled = false;
    }

    /// <summary>
    /// Activate an animation
    /// </summary>
    /// <param name="state">Animation asociete to this Zombiestate</param>
    /// <param name="blend">Blend percentaje to enable blendtree</param>
    public void SetState(ZombieState state,float blend=1.0f) {
        if(p_Animator.runtimeAnimatorController == null)
            throw new System.InvalidOperationException("Error, this animator hasn't got a runtime animator.");

        p_State = state;
        p_Animator.SetFloat("Speed", blend);
        p_Animator.SetInteger("State", (int)p_State);
    }

    /// <summary>
    /// Throw an OnscreamEnd event
    /// </summary>
    public void EndOfScream() {
        OnScreamEnd?.Invoke();
    }
}
