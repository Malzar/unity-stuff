﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieColliderControler : MonoBehaviour {

    public delegate void HitRecived(float damage);
    public event HitRecived OnHitRecived;

    private GameObject p_particles;

    public void SetBloodPArticles(GameObject particles) {
        p_particles = particles;
    }

    public void HitZombie(float damage, Vector3 position) {
        OnHitRecived?.Invoke(damage);
        GameObject particles = Instantiate(p_particles, transform, false);

        Destroy(particles, 1.0f);
    }
}
