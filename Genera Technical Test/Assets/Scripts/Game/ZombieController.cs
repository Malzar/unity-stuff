﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ZombieState {
    Move = 0,
    Stop,
    Atack,
    Death,
    Death2,
}

public class ZombieController : MonoBehaviour {

    public delegate void ZombieDead(ZombieController zombie);
    public event ZombieDead OnZombieDead;

    public delegate void ZombieAttack(float damage);
    public event ZombieAttack OnZombieAttack;

    [SerializeField]
    private List<ZombieColliderControler> p_Colliders;
    [SerializeField]
    private ZombieMovementController p_MovementController;
    [SerializeField]
    private ZombieAnimatorController p_Animator;
    private float p_ZombieLife;
    private float p_ZombieDamage;
    private float p_ZombieVelocity;
    private float p_ZombieDamagePerAttack;
    private float p_TimeElipsed;
    private ZombieState p_ZombieState;
    private bool p_ZombieScreaming;
    private bool p_ZombieEnable;

    public bool ZombieEnable { get { return p_ZombieEnable; } set { p_ZombieEnable = value; } }

    private void Awake() {
        Initialize();
    }

    private void Initialize() {
        if(p_ZombieEnable == false) {
            p_ZombieState = ZombieState.Stop;
        }        
        p_ZombieScreaming = false;
        p_TimeElipsed = 0;
    }

    public void InitializeComponents(ScriptablePath path, ScriptableZombie propierties, RuntimeAnimatorController animator) {
        p_ZombieDamage = 0;
        p_ZombieVelocity = propierties.Velocity;
        p_ZombieDamagePerAttack = propierties.DamagePerAttack;
        p_ZombieLife = propierties.Life;
        p_MovementController.SetPath(path);
        p_MovementController.SetMovementVelocity(p_ZombieVelocity);
        p_Animator.SetAnimatorController(animator);
        p_ZombieState = ZombieState.Move;
        p_ZombieEnable = true;
        foreach(ZombieColliderControler collider in p_Colliders) {
            collider.OnHitRecived += MakeDamage;
            collider.SetBloodPArticles(propierties.Particles);
        }
    }

    // Update is called once per frame
    void Update() {
        if(p_ZombieEnable) {
            switch(p_ZombieState) {
                case ZombieState.Move:
                Movezombie();
                break;
                case ZombieState.Stop:
                Scream();
                break;
                case ZombieState.Atack:
                Attack();
                break;
                case ZombieState.Death:
                case ZombieState.Death2:
                Death();
                break;
            }

        }
    }

    private void MakeDamage(float damage) {
        p_ZombieDamage += damage;
        if(p_ZombieDamage >= p_ZombieLife) {
            p_ZombieState = ZombieState.Death;
        } else {
            p_ZombieState = ZombieState.Stop;
        }
    }

    private void Death() {
        p_Animator.SetState(p_ZombieState);
        OnZombieDead?.Invoke(this);
        p_ZombieEnable = false;
        Destroy(gameObject, 5.0f);
    }

    private void Scream() {
        if(!p_ZombieScreaming) {
            p_Animator.SetState(p_ZombieState);
            p_ZombieScreaming = true;
        }else if(!p_Animator.IsScreamAnimation()) {
            p_ZombieState = ZombieState.Move;
            p_ZombieScreaming = false;
        }
    }

    private void Attack() {
        if(p_TimeElipsed == 0) {
            p_Animator.SetState(p_ZombieState, p_ZombieLife / p_ZombieDamage);
            OnZombieAttack?.Invoke(p_ZombieDamagePerAttack);
            p_TimeElipsed += Time.deltaTime;
        } else {
            p_TimeElipsed += Time.deltaTime;
            if(p_TimeElipsed > 2.0f) {
                p_TimeElipsed = 0;
            }
        }
    }

    private void Movezombie() {
        float movementAnimation = (p_ZombieLife - p_ZombieDamage) / p_ZombieLife;
        bool canMove;
        //I use a quadratic root to reduce the velocity slower than lineal.
        float MovementVelocity = p_ZombieVelocity * Mathf.Max(movementAnimation, 0.2f);

        p_MovementController.SetMovementVelocity(MovementVelocity);
        canMove = p_MovementController.UpdateMovement(Time.deltaTime);
        p_Animator.SetState(p_ZombieState, movementAnimation);

        if(!canMove)
            p_ZombieState = ZombieState.Atack;
    }
}
