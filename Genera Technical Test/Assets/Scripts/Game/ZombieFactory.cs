﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieFactory : MonoBehaviour
{
    public static ZombieFactory Instance;

    public List<ScriptablePath> Paths;
    public List<ScriptableZombie> ZombiesPropierties;
    public List<GameObject> ZombiesPrefabs;
    public List<RuntimeAnimatorController> Animators;

    private void Awake() {
        if(Instance == null) {
            Instance = this;
        }else if(Instance != this) {
            Destroy(gameObject);
        }
    }

    public ZombieController SpawnZombie() {
        if(Paths.Count == 0 || ZombiesPropierties.Count == 0 || ZombiesPrefabs.Count == 0) {
            throw new System.MissingMemberException("Path, zombies propierties or zobies prefabs are empty.");
        }

        ScriptablePath path = Paths [Random.Range(0, Paths.Count)];
        ScriptableZombie propierties = ZombiesPropierties [Random.Range(0, ZombiesPropierties.Count)];
        GameObject prefab = ZombiesPrefabs [Random.Range(0, ZombiesPrefabs.Count)];
        Vector3 spawn = path.Waypoints [0];

        GameObject zombieGO = GameObject.Instantiate(prefab, spawn, Quaternion.identity, this.transform);
        ZombieController controller = zombieGO.GetComponent<ZombieController>();

        RuntimeAnimatorController animator = Animators [(int)propierties.Mode];

        controller.InitializeComponents(path, propierties,animator);

        return controller;
    }
}
