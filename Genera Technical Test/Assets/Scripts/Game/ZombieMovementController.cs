﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieMovementController : MonoBehaviour {
    private float p_DistanceTraveled;
    private bool p_CanMove;
    private float p_MovementVelocity;
    private ScriptablePath p_Path;
    
    public Transform ZombieObject;

    void Awake() {
        Initialize();
    }

    private void Initialize() {
        if(ZombieObject == null)
            ZombieObject = transform;
        p_CanMove = true;
        p_DistanceTraveled = 0;
        p_MovementVelocity = 0;
    }

    public void SetPath(ScriptablePath path) {
        p_Path = path;
    }

    public void SetMovementVelocity(float velocity) {
        p_MovementVelocity = velocity;
    }

    /// <summary>
    /// Update position and direction of the game object
    /// </summary>
    /// <param name="time">delta time</param>
    /// <returns>true when the path isn't finished
    ///         false when the path is ended</returns>
    public bool UpdateMovement(float time) {
        if(p_Path== null)
            throw new System.InvalidOperationException("Error, this zombie movement controller hasn't got a path.");

        bool canMove;
        int waypoint;
        float step;

        p_DistanceTraveled += p_MovementVelocity * time;
        canMove = GetNormalizedPostion(p_DistanceTraveled, out waypoint, out step);
        ZombieObject.position = GetPathPosition(waypoint, step);
        ZombieObject.forward = GetPathDirection(waypoint, step);

        return canMove;
    }

    /// <summary>
    /// Get the waypoint and step from the spline path.
    /// </summary>
    /// <param name="distance">distance went down </param>
    /// <param name="waypoint">current waypoint of spline path</param>
    /// <param name="step">current step of spline path</param>
    /// <returns>True when the distance isn't higher than the spline
    ///          False when the distance is higher than the spline</returns>
    private bool GetNormalizedPostion(float distance, out int waypoint, out float step) {
        waypoint = 0;
        if(distance > p_Path.PathLenght) {
            waypoint = p_Path.SplinesLengths.Count - 1;
            step = 1;
            return false;
        } else {
            while(distance > p_Path.SplinesLengths [waypoint]) {
                distance -= p_Path.SplinesLengths [waypoint];
                waypoint++;
            }
            step = distance / p_Path.SplinesLengths [waypoint];
            return true;
        }
    }

    private Vector3 GetPathDirection(int waypoint, float step) {
        return Bezier.CalculateDirectionInterpolation(p_Path.Waypoints [waypoint],
            p_Path.ControlWaypoints [2 * waypoint],
            p_Path.ControlWaypoints [2 * waypoint + 1],
            p_Path.Waypoints [waypoint + 1], step);
    }

    private Vector3 GetPathPosition(int waypoint, float step) {

        return Bezier.CalculateBecierInterpolation(p_Path.Waypoints [waypoint],
            p_Path.ControlWaypoints [2 * waypoint],
            p_Path.ControlWaypoints [2 * waypoint + 1],
            p_Path.Waypoints [waypoint + 1], step);
    }
}
