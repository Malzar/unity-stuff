﻿using UnityEngine;

public class Bezier {
    public static Vector3 CalculateBecierInterpolation(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {
        Vector3 interpolation;
        float oneMinusT = 1 - t;
        interpolation = p0 * oneMinusT * oneMinusT * oneMinusT
                        + 3.0f * p1 * t * oneMinusT * oneMinusT
                        + 3.0f * p2 * t * t * oneMinusT
                        + p3 * t * t * t;
        return interpolation;
    }

    public static Vector3 CalculateVelocityInterpolation(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {
        Vector3 velocity;
        float oneMinusT = 1 - t;

        velocity = 3.0f*(oneMinusT*oneMinusT) *(p1-p0)+
                    6.0f*oneMinusT*t*(p2-p1)+
                    3.0f*t*t*(p3-p2);

        return velocity;
    }

    public static Vector3 CalculateDirectionInterpolation(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {

        return Vector3.Normalize(CalculateVelocityInterpolation(p0, p1, p2, p3, t));

    }

    

}
