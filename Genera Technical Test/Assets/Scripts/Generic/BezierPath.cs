﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BezierPath : MonoBehaviour {
    [SerializeField, HideInInspector]
    private List<Vector3> p_Waypoints;
    [SerializeField, HideInInspector]
    private List<Vector3> p_ControlWaypoints;
    [SerializeField, HideInInspector]
    private List<float> p_SplinesLengths;
    [SerializeField, HideInInspector]
    private float p_PathLegth;
    [SerializeField]
    private Transform p_SpawnPoint;

    private const float CONTROLPOINTSEPARATION = 2.0f;
    private const float WAYPOINTSEPARATION = 10.0f;
    private const int SEGMENTBEZIERCOUNT = 100;

    public Transform SpawnPoint { get { return p_SpawnPoint; } }
    public int SegmentCount { get { return SEGMENTBEZIERCOUNT; } }
    public List<Vector3> Waypoints { get { return p_Waypoints; } }
    public List<Vector3> ControlWaypoints { get { return p_ControlWaypoints; } }
    public List<float> SplinesLegths { get { return p_SplinesLengths; } }
    public float PathLegth { get { return p_PathLegth; } }

    private void Awake() {
    }

    public void Initialize() {
        p_Waypoints = new List<Vector3>();
        p_ControlWaypoints = new List<Vector3>();
        //create first waypoint
        Vector3 waypoint = Vector3.zero;
        p_Waypoints.Add(waypoint);
        //create first controll waypoint
        Vector3 controllWayPoint = waypoint + Vector3.left* CONTROLPOINTSEPARATION;
        p_ControlWaypoints.Add(controllWayPoint);
        //create second waypoint
        waypoint = transform.position + transform.forward* WAYPOINTSEPARATION;
        p_Waypoints.Add(waypoint);
        //create second controll waypoint
        controllWayPoint = waypoint+Vector3.Cross(transform.forward, Vector3.down)* CONTROLPOINTSEPARATION;
        p_ControlWaypoints.Add(controllWayPoint);
    }

    public void AddWayPoint() {
        if (p_SpawnPoint == null) {
            Debug.LogWarning("This path needs a spawn point");
        }
        //generate way point
        Vector3 dir = p_Waypoints[p_Waypoints.Count - 1] - p_Waypoints[p_Waypoints.Count - 2];
        Vector3 point = p_Waypoints[p_Waypoints.Count - 1] + dir.normalized*WAYPOINTSEPARATION;
        p_Waypoints.Add(point);

        //generate controll points
        dir = p_Waypoints[p_Waypoints.Count - 2] - p_ControlWaypoints[p_ControlWaypoints.Count-1];
        point = dir.normalized * CONTROLPOINTSEPARATION + p_Waypoints[p_Waypoints.Count - 2];
        p_ControlWaypoints.Add(point);
        dir = p_Waypoints[p_Waypoints.Count - 1] - p_Waypoints[p_Waypoints.Count - 2];
        dir = Vector3.Cross(dir.normalized, Vector3.down);
        point = dir* CONTROLPOINTSEPARATION + p_Waypoints[p_Waypoints.Count -1];
        p_ControlWaypoints.Add(point);
    }

    public void LoadScriptablePath(ScriptablePath data) {
        p_ControlWaypoints = new List<Vector3>();
        p_Waypoints = new List<Vector3>();
        p_ControlWaypoints = data.ControlWaypoints;
        p_Waypoints = data.Waypoints;
    }

    public void ResetWayPoint() {
        for(int i=1; i < p_Waypoints.Count - 1; i++) {
            RemoveWayPoint(i);
            i--;
        }

    }

    public void RemoveWayPoint(int index) {
        if (index < 0 || index >= p_Waypoints.Count)
            throw new System.ArgumentException("This waypoint doesn't exit");
        if (p_Waypoints.Count > 2) {
            //remove controll points
            if (index == 0) {
                p_ControlWaypoints.RemoveAt(0);
                p_ControlWaypoints.RemoveAt(0);
            } else if (index == p_Waypoints.Count - 1) {
                p_ControlWaypoints.RemoveAt(p_ControlWaypoints.Count-1);
                p_ControlWaypoints.RemoveAt(p_ControlWaypoints.Count-1);
            } else {
                p_ControlWaypoints.RemoveAt(2 * index);
                p_ControlWaypoints.RemoveAt(2 * index-1);
            }
            //remove waypoint
            p_Waypoints.RemoveAt(index);
        }
    }

    public void SetWayPoint(int index, Vector3 position) {
        Vector3 offset = position - p_Waypoints[index];
        p_Waypoints[index] = position;
        if (index * 2 - 1 >= 0)
            p_ControlWaypoints[index * 2-1] += offset;
        if (index * 2 < p_ControlWaypoints.Count)
            p_ControlWaypoints[index * 2] += offset;
    }

    public void SetSpawnPoint() {
        if (p_SpawnPoint != null) {
            Vector3 offset = p_SpawnPoint.transform.position - p_Waypoints[0];
            p_Waypoints[0] += offset;
            p_ControlWaypoints[0] += offset;
        }
    }
    
    public void SetControllWayPoint(int index, Vector3 position) {
        p_ControlWaypoints[index] = position;
        if (index > 0 && index < (p_ControlWaypoints.Count - 1)) {
            int wayPoint = (index + 1) / 2;
            int opossiteControllWayPoint = index % 2;
            if (opossiteControllWayPoint == 0) {
                opossiteControllWayPoint = index - 1;
            } else {
                opossiteControllWayPoint = index + 1;
            }
            float distance = Vector3.Magnitude(p_ControlWaypoints[opossiteControllWayPoint] - p_Waypoints[wayPoint]);
            Vector3 direction = p_Waypoints[wayPoint] - position;
            p_ControlWaypoints[opossiteControllWayPoint] = (direction.normalized) * distance + p_Waypoints[wayPoint];
        }
    }

    public void CalculatePathLegth() {
        if(p_SplinesLengths==null) {
            p_SplinesLengths = new List<float>();
        } else {
            p_SplinesLengths.Clear();
        }

        p_PathLegth=0;
        float legth;
        for(int i = 0; i < p_Waypoints.Count - 1; i++) {
            legth = ArcLegth(p_Waypoints [i], p_ControlWaypoints [2 * i],
                p_ControlWaypoints [2 * i + 1], p_Waypoints [i + 1], SEGMENTBEZIERCOUNT);
            p_PathLegth += legth;
            p_SplinesLengths.Add(legth);
        }

    }

    private float ArcLegth(Vector3 A, Vector3 B, Vector3 CPA, Vector3 CPB, int steps) {
        float length=0;
        float stepIncrement = (float)1.0/steps;
        float percentaje;
        Vector3 oldPos=A;
        Vector3 newPos;

        for(int i=1; i < steps; i++) {
            percentaje = i * stepIncrement;
            newPos = Bezier.CalculateBecierInterpolation(A, CPA, CPB, B, percentaje);
            length += Vector3.Distance(newPos, oldPos);
            oldPos = newPos;
        }

        return length;
    }
}
