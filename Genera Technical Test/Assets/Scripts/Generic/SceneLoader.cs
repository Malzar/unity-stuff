﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SCENES {
    Menu,
    Level1,
    Level2
}

public class SceneLoader : MonoBehaviour {

    public static SceneLoader Instance;

    [SerializeField]
    private List<string> p_ScenesNames;

    private string p_SceneToLoad;
    private bool p_Loading;

    private void Awake() {

        if (Instance == null) {
            Instance = this;
        } else if (Instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        InitializeValues();        
    }

    private void InitializeValues() {
        p_Loading = false;
        p_SceneToLoad = "";
    }

    public string GetSceneName(SCENES scene) {
        if((int)scene>= Enum.GetNames(typeof(SCENES)).Length) 
            throw new System.IndexOutOfRangeException("This scene not saved into list names");
        return p_ScenesNames [(int) scene];
    }

    public SCENES GetLevelScene(int level) {
        SCENES scene;

        switch(level) {
            case 0:
            scene = SCENES.Level1;
            break;
            case 1:
            scene = SCENES.Level2;
            break;
            default:
            scene = SCENES.Menu;
            break;
        }

        return scene;
    }

    public void LoadScene(SCENES scene) {
        if (!p_Loading) {
            p_Loading = true;
            switch(scene) {
                case SCENES.Menu:
                p_SceneToLoad = p_ScenesNames[0];
                break;
                case SCENES.Level1:
                p_SceneToLoad = p_ScenesNames [1];
                break;
                case SCENES.Level2:
                p_SceneToLoad = p_ScenesNames [2];
                break;
            }
            LoadScene();
        }
    }

    public void LoadScene(SCENES scene, float delay) {
        if (!p_Loading) {
            p_Loading = true;
            switch(scene) {
                case SCENES.Menu:
                p_SceneToLoad = p_ScenesNames [0];
                break;
                case SCENES.Level1:
                p_SceneToLoad = p_ScenesNames [1];
                break;
                case SCENES.Level2:
                p_SceneToLoad = p_ScenesNames [2];
                break;
            }
            Invoke("LoadScene", delay);
        }
    }

    private void LoadScene() {
        SceneManager.LoadScene(p_SceneToLoad, LoadSceneMode.Single);
        p_Loading = false;
    }
}
