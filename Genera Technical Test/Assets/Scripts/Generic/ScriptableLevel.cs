﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Level propierties",menuName ="Mechanic/Level propierties", order = 2)]
public class ScriptableLevel : ScriptableObject
{
    public int TotalZombieSpawned;
    public int LevelsTimeInSecond;
    public float PlayerLife;
}
