﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "path", menuName = "Mechanic/path", order = 0)]
public class ScriptablePath : ScriptableObject {

    public List<Vector3> ControlWaypoints;
    public List<Vector3> Waypoints;
    public List<float> SplinesLengths;
    public float PathLenght;

}
