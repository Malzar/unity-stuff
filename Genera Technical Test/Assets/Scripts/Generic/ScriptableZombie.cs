﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WalkMode {
    Up=0,
    Crawl
}

[CreateAssetMenu(fileName = "Zombie Propierties", menuName = "Mechanic/Zombie", order = 1)]
public class ScriptableZombie : ScriptableObject {
    public float Velocity;
    public float Life;
    public WalkMode Mode;
    public float DamagePerAttack;
    public GameObject Particles;
}
