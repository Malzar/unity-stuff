﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
   public void PlayGame() {
        GameMechanic.Instance.SetLevelToLoad(0);
        SceneLoader.Instance.LoadScene(SCENES.Level1);
   }
}
